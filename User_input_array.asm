include "emu8086.inc"
.model small
.stack 100h
.data
    arr db 50 dup(?)
.code
main proc
    mov ax,@data
    mov ds,ax
    
    xor bx,bx
    xor cx,cx
    
    print "How many numbers you want to add: "
    
    mov ah,01h
    int 21h
    and al,0fh
    
    mov cl,al
    mov bl,al
    mov si,0
    
    printn
    print "Enter values: "
    printn
    
    Input:
    int 21h
    mov arr[si],al
    inc si
    loop input
    
    printn
    print "Output"
    printn
    
    mov cx,bx
    mov si,0
    mov ah,2
    output:
    mov dl,arr[si]
    int 21h
    inc si
    loop output
    
    main endp
end main