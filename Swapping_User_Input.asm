.model small
.stack 100h
.data

.code
main proc
    call abc
    
    mov ah,4ch
    int 21h
        
        main endp

    abc proc
        
        mov ah,01h
        int 21h
        
        xor ch,ch
        push ax
        
        mov ah,01h
        int 21h
        
        xor ch,ch
        mov bx,ax
        push bx
        
        pop ax
        pop bx
        
        mov ah,02h
        mov dx,ax
        int 21h
        
        mov ah,02h
        mov dx,bx
        int 21h
        
        abc endp
end main