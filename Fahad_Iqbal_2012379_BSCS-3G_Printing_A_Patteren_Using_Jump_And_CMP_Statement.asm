.model small
.stack 100h
.data
str db 0
line db 1
NL db 0DH,0AH,"$"
.code
main proc
    mov ax,@data
    mov ds,ax
    
    print_star:
    mov ah,2
    mov dl, '*'
    int 21h
    
    inc str
    
    mov bl,str
    cmp bl,line
    je newline
    jmp print_star
    
    newline:
    mov ah,9
    lea dx,NL
    int 21h
    
    cmp bl,5
    je exit
    
    inc line 
    mov str,0
    jmp print_star
    
    exit:
    mov ah,4ch
    int 21h

    main endp
end main

