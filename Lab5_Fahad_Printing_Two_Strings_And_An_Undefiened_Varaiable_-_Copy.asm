.model small
.stack 100h
.data 
var1 db "Fahad $"
var2 dw "Iqbal $"
var3 db ?  
.code
main proc
mov ax,@data 
mov ds,ax

mov dl,offset var1
mov ah,09h
int 21h

mov dx,offset var2
mov ah,09h
int 21h 

mov var3,'5'
mov dl,var3
mov ah,02h
int 21h

mov ah,4ch
int 21h    
        
main endp
end main




